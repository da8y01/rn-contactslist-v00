import React from 'react';
import { Button, Text, View } from 'react-native';

export default class ContactDetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: navigation.getParam('name'),
    };
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerTitle: this.props.route.params.contact.name,
    });
  }

  // componentWillUpdate() {
  //   this.props.navigation.setOptions({
  //     headerTitle: this.props.route.params.contact.name,
  //   });
  // }

  render() {
    return (
      <View>
        {/* <Text>{this.props.navigation.getParam('phone')}</Text> */}
        <Text>{this.props.route.params.contact.phone}</Text>
        <Button title="Go to random contact" onPress={this.goToRandomContact} />
      </View>
    );
  }

  goToRandomContact = () => {
    // const { contacts } = this.props.screenProps;
    const contacts = this.props.route.params.contacts;
    // const phone = this.props.navigation.getParam('phone');
    const phone = this.props.route.params.contact.phone;
    let randomContact;
    while (!randomContact) {
      const randomIndex = Math.floor(Math.random() * contacts.length);
      if (contacts[randomIndex].phone !== phone) {
        randomContact = contacts[randomIndex];
      }
    }

    // this.props.navigation.navigate('ContactDetails', {
    //   ...randomContact,
    // });
    this.props.navigation.push('ContactDetails', {
    // this.props.navigation.navigate('ContactDetails', {
      contact: {...randomContact},
      contacts
    });
  };
}
