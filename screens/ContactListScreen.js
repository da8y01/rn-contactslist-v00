import React from 'react';
import { Button, View, StyleSheet } from 'react-native';
import {connect} from 'react-redux'

import SectionListContacts from '../SectionListContacts';

class ContactListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Contacts',
      headerRight: (
        <Button
          title="Add"
          onPress={() => navigation.navigate('AddContact')}
          color="#a41034"
        />
      ),
    };
  };

  componentDidMount() {
    this.props.navigation.setOptions({
      headerRight: () => (
        <Button
          title="Add"
          onPress={() => this.props.navigation.navigate('AddContact', {addContact: this.props.route.params.addContact})}
          color="#a41034"
        />
      ),
    });
  }

  state = {
    showContacts: true,
  };

  toggleContacts = () => {
    this.setState(prevState => ({ showContacts: !prevState.showContacts }));
  };

  handleSelectContact = contact => {
    // this.props.navigation.push('ContactDetails', contact);
    this.props.navigation.push('ContactDetails', {contact, contacts: this.props.route.params.contacts});
    // this.props.navigation.navigate('ContactDetails', {contact, contacts: this.props.route.params.contacts});
  };

  render() {
    return (
      <View style={styles.container}>
        <Button title="toggle contacts" onPress={this.toggleContacts} />
        {this.state.showContacts && (
          <SectionListContacts
            // contacts={this.props.screenProps.contacts}
            contacts={this.props.route.params.contacts}
            onSelectContact={this.handleSelectContact}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const mapStateToProps = state => ({
  contacts: state.contacts,
})

export default connect(mapStateToProps)(ContactListScreen)
