// import Example from './examples/0-Switch';
// import Example from './examples/1-Stack';
// export default Example;

import React from "react";
import { StatusBar, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from "react-native-vector-icons/Ionicons";
import {Provider} from 'react-redux'

import AddContactScreen from "./screens/AddContactScreen";
import SettingsScreen from "./screens/SettingsScreen";
import ContactListScreen from "./screens/ContactListScreen";
import ContactDetailsScreen from "./screens/ContactDetailsScreen";
import LoginScreen from "./screens/LoginScreen";
import {fetchUsers} from './api'
import contacts from './contacts'
import store from './redux/store'

// const MainStack = createNativeStackNavigator(
//   {
//     ContactList: ContactListScreen,
//     ContactDetails: ContactDetailsScreen,
//     AddContact: AddContactScreen
//   },
//   {
//     initialRouteName: "ContactList",
//     navigationOptions: {
//       headerTintColor: "#a41034",
//       headerStyle: {
//         backgroundColor: "#fff"
//       }
//     }
//   }
// );
const MainStack = createNativeStackNavigator();

function MainStackNavigator({route}) {
  return (
    // <NavigationContainer>
      <MainStack.Navigator initialRouteName="ContactList"
        options={{
          headerTintColor: "#a41034",
          headerStyle: {
            backgroundColor: "#fff"
          }
        }}
      >
        <MainStack.Screen name="ContactList" component={ContactListScreen} initialParams={{contacts: contacts}} />
        <MainStack.Screen name="ContactDetails" component={ContactDetailsScreen} />
        <MainStack.Screen name="AddContact" component={AddContactScreen} />
      </MainStack.Navigator>
    // </NavigationContainer>
  );
}
MainStack.navigationOptions = {
  tabBarIcon: ({ focused, tintColor }) => (
    <Ionicons
      name={`ios-contacts${focused ? "" : "-outline"}`}
      size={25}
      color={tintColor}
    />
  )
};

// const MainTabs = createBottomTabNavigator(
//   {
//     Contacts: MainStack,
//     Settings: SettingsScreen
//   },
//   {
//     tabBarOptions: {
//       activeTintColor: "#a41034"
//     }
//   }
// );
const MainTabs = createBottomTabNavigator();

function MainTabsNavigator({route}) {
  return (
    // <NavigationContainer>
      <MainTabs.Navigator initialRouteName="Contacts"
        options={{
          tabBarOptions: {
            activeTintColor: "#a41034"
          }
        }}
      >
        <MainTabs.Screen name="Contacts" component={MainStackNavigator} />
        <MainTabs.Screen name="Settings" component={SettingsScreen} />
      </MainTabs.Navigator>
    // </NavigationContainer>
  );
}

// const AppNavigator = createSwitchNavigator({
//   Login: LoginScreen,
//   Main: MainTabs
// });
const AppNavigator = createNativeStackNavigator();

function AppNavigatorNav() {
  return (
    <NavigationContainer>
      <AppNavigator.Navigator>
        <AppNavigator.Screen name="Login" component={LoginScreen} />
        <AppNavigator.Screen name="Main" component={MainTabsNavigator}
          initialParams={{
            contacts: this.state.contacts,
            addContact: this.addContact
          }}
        />
      </AppNavigator.Navigator>
    </NavigationContainer>
  );
}
class AppNavigatorCls extends React.Component {
  state = {
    contacts: null
  };

  render() {
    return (
      <NavigationContainer>
        <MainStack.Navigator initialRouteName="Login">
          <MainStack.Screen name="Login" component={LoginScreen} />
          <MainStack.Screen name="Main" component={MainTabsNavigator} />
        </MainStack.Navigator>
      </NavigationContainer>
    );
  }
}

export default class App extends React.Component {
  state = {
    contacts: contacts,
  };

  componentDidMount() {
    // this.getUsers()
  }

  getUsers = async () => {
    const results = await fetchUsers()
    this.setState({contacts: results})
  }

  addContact = newContact => {
    console.log(newContact)
    this.setState(prevState => ({
      contacts: [...prevState.contacts, newContact]
    }));
  };

  render() {
    return (
      <Provider store={store}>
        <AppNavigatorCls />
      </Provider>
    );
  }
}
